
const obj1 = {
  a: {
      b: 1,
  },
};
const obj2 = {
  a: {
      b: 2,
  },
};
const obj3 = {
  a: {
      b: {
          c: 3,
          },
      d:{
          z:22,
        }
      },

};

const obj4 = {
  a: {
      b: 1,
  },
};

const isObject = (object) => {
    return object != null && typeof object === "object";
  };

function deepEqual (elem1, elem2, errPath = '') {
  var res = true,
      nPath = errPath;
  
    if (elem1 === elem2) {
        return true;
    }

    if (isObject(elem1) && isObject(elem2)) {
        if(Object.keys(elem1).length == Object.keys(elem2).length) {
            for(let key of Object.keys(elem1)) {
                if(elem2.hasOwnProperty(key) != true) {
                  nPath += '.' + key;
                  return { res:false, nPath };                  
                }
            }
            for(let key of Object.keys(elem1)) {
                if(isObject(elem1[key]) && isObject(elem2[key])) {
                    return deepEqual(elem1[key], elem2[key], nPath + '.' + key);
                }
                else {
                  if(elem1[key] !== elem2[key]) {
                    nPath += '.' + key;
                      return { res:false, nPath };                      
                  }
                }
            }
        }
         else return { res:false, nPath };
    }        
      else return { res:false, nPath };       
      
    return true;
}

const {res, nPath} = deepEqual(obj1, obj2);
res ?  console.log('OK') : console.log(`Error path: ${nPath}`);
